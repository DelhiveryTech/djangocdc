
Tracking full model history
===============================

In order to enable historic tracking on a model, the model needs to have a
property of type ``django_cdc.models.managers.DjangoCDC`` attached::


    from django.db import models
    from django_cdc.models.fields import LastUserField
    from django_cdc.models.managers import DjangoCDC


    class ProductCategory(models.Model):
        name = models.CharField(max_length=150, primary_key = True)
        description = models.TextField()

        django_cdc = DjangoCDC()

    class Product(models.Model):
        name = models.CharField(max_length = 150)
        description = models.TextField()
        price = models.DecimalField(max_digits = 10, decimal_places = 2)
        category = models.ForeignKey(ProductCategory)

        django_cdc = DjangoCDC()


Each time you add an instance of DjangoCDC to any of your models you need to run
``python manage.py syncdb`` so that the database table that keeps the actual
audit log for the given model gets created.


Querying the audit log
-------------------------------

An instance of ``django_cdc.models.managers.DjangoCDC`` will behave much like a
standard manager in your model. Assuming the above model
configuration you can go ahead and create/edit/delete instances of Product,
to query all the changes that were made to the products table
you would need to retrieve all the entries for the audit log for that
particular model class::

    In [2]: Product.django_cdc.all()
    Out[2]: [<ProductDjangoCDCEntry: Product: My widget changed at 2011-02-25 06:04:29.292363>,
            <ProductDjangoCDCEntry: Product: My widget changed at 2011-02-25 06:04:24.898991>,
            <ProductDjangoCDCEntry: Product: My Gadget super changed at 2011-02-25 06:04:15.448934>,
            <ProductDjangoCDCEntry: Product: My Gadget changed at 2011-02-25 06:04:06.566589>,
            <ProductDjangoCDCEntry: Product: My Gadget created at 2011-02-25 06:03:57.751222>,
            <ProductDjangoCDCEntry: Product: My widget created at 2011-02-25 06:03:42.027220>]

Accordingly you can get the changes made to a particular model instance like so::

    In [4]: Product.objects.all()[0].django_cdc.all()
    Out[4]: [<ProductDjangoCDCEntry: Product: My widget changed at 2011-02-25 06:04:29.292363>,
            <ProductDjangoCDCEntry: Product: My widget changed at 2011-02-25 06:04:24.898991>,
            <ProductDjangoCDCEntry: Product: My widget created at 2011-02-25 06:03:42.027220>]

Instances of ``DjangoCDC`` behave like django model managers and can be queried in the same fashion.

The querysets yielded by ``DjangoCDC`` managers are querysets for models
of type ``[X]DjangoCDCEntry``, where X is the tracked model class.
An instance of ``XDjangoCDCEntry`` represents a log entry for a particular model
instance and will have the following fields that are of relevance:

    * ``action_id`` - Primary key for the log entry.
    * ``action_date`` - The point in time when the logged action was performed.
    * ``action_user`` - The user that performed the logged action.
    * ``action_type`` - The type of the action (Created/Changed/Deleted)
    * Any field of the original ``X`` model that is tracked by the audit log.


M2M Relations
--------------------

Tracking changes on M2M Relations doesn't work for now. If you really need to track changes on M2M relations with
this package, explicitly define the table with another model instead of declaring the M2M relation.

Abstract Base Models
--------------------------

For now just attaching the ``DjangoCDC`` manager to an abstract base model won't make it automagically attach itself on the child
models. Just attach it to every child separately.

Disabling/Enabling Tracking on a Model Instance
-------------------------------------------------
There may be times when you want a certain ``save()`` or ``delete()`` on a model instance to be ignored by the audit log.
To disable tracking on a model instance you simply call::

    modelinstance.django_cdc.disable_tracking()

To re-enable it do::

    modelinstance.django_cdc.enable_tracking()

Note that this only works on instances, trying to do that on a model class will raise an exception.
