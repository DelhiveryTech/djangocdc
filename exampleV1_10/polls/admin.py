from django.contrib import admin

from .models import Question


admin.site.register(Question)


def has_edit_permission(self, request):
    return True
# Register your models here.

