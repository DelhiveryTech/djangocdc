from django.db import models
from django_cdc.models.managers import DjangoCDC
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.db import models
from django_cdc.models.constants import ServiceType


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    django_cdc = DjangoCDC(foreign_keys={'question': ['question_text', 'pub_date']},
                           services=[ServiceType.SNS, ServiceType.KINESIS])


class QC(models.Model):
    choice_value = models.ForeignKey(Choice)
    question_value = models.ForeignKey(Question)
    name = models.CharField(max_length=200)
    django_cdc = DjangoCDC(
        foreign_keys={'choice_value': ['choice_text', 'votes'], 'question_value': ['question_text', 'pub_date']},
        services=[ServiceType.KINESIS])
